<?php

/**
 * @file
 * Contains \Drupal\expandFormatter\Plugin\field\formatter\expandFormatter.
 */
namespace Drupal\expandFormatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'expandFormatter' formatter
 *
 * @FieldFormatter(
 *   id = "expandFormatter",
 *   label = @Translation("Expand formatter"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   },
 *   settings = {
 *     "trim_length" = "600",
 *     "expandLabel" = "Read more",
 *     "collapseLabel" = "Read less",
 *     "trim_element" = "letter"
 *   },
 *    edit = {
 *      "editor" = "form"
 *    }
 * )
 */
class expandFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items) {
    drupal_add_js(drupal_get_path('module', 'expandFormatter') . '/expander.js', 'file');

    $elements = array();

    // Get settings.
    $expandLabel = $this->getSetting('expandLabel');
    $collapseLabel = $this->getSetting('collapseLabel');
    $trimLength = $this->getSetting('trim_length');
    $trimElement = $this->getSetting('trim_element');

    foreach ($items as $delta => $item) {
      // Get Text.
      $value = $item->getValue();

      // Split Text.
      $text = $this->splitText(strip_tags($value['value']), $trimLength, $trimElement);

      // Create triggers.
      $expandTrigger = '<a href="javascript:void(0)" class="expandtrigger">' . $expandLabel . '</a>';
      $collapseTrigger = '<a href="javascript:void(0)" class="collapsetrigger hidden">' . $collapseLabel . '</a>';

      // Create teaser and expand-text.
      $teaser = '<span class="teaser">' . $text['Teaser'] . '</span>';
      $expandText = '<span class="expandtext hidden">' . $text['ExpandText'] . '</span>';

      // Create Output.
      $output = '<span class="expand-formatter">' . $teaser . ' ' . $expandTrigger . $expandText . ' ' . $collapseTrigger . '</span>';

      // Set element markup.
      $elements[$delta] = array('#markup' => $output);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, array &$form_state) {
    $element['expandLabel'] = array(
      '#title' => t('Expand-label'),
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => $this->getSetting('expandLabel'),
      '#required' => TRUE,
    );
    $element['collapseLabel'] = array(
      '#title' => t('Collapse-label'),
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => $this->getSetting('collapseLabel'),
      '#required' => FALSE,
    );
    $element['trim_length'] = array(
      '#title' => t('Trim-length'),
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $this->getSetting('trim_length'),
      '#format' => 'number',
      '#required' => TRUE,
      '#suffix' => 'letter(s)'
    );
    $element['trim_element'] = array(
      '#title' => t('Trim after'),
      '#type' => 'select',
      '#options' => array(
        'letter' => 'letter',
        'word' => 'word',
        'sentece' => 'sentece',
      ),
      '#default_value' => $this->getSetting('trim_element'),
      '#required' => TRUE,
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $summary[] = t('@setting: @value', array('@setting' => 'Expand Label', '@value' => $this->getSetting('expandLabel')));
    $summary[] = t('@setting: @value', array('@setting' => 'Collapse Label', '@value' => $this->getSetting('collapseLabel')));
    $summary[] = t('@setting: @value', array('@setting' => 'Trim-length', '@value' => $this->getSetting('trim_length') . ' letter(s)'));
    $summary[] = t('@setting: @value', array('@setting' => 'Trim after', '@value' => $this->getSetting('trim_element')));


    return $summary;
  }

  /**
   *
   * Function to split the text into text, teaser and expand-trigger.
   *
   * @param type $text
   * @param type $trimLength
   * @param type $trimElement
   *
   * @return type
   */
  private function splitText($text, $trimLength, $trimElement) {
    if ($trimElement === 'letter') {
      $teaser = substr($text, 0, $trimLength);
      $expandText = substr($text, strlen(substr($text, 0, $trimLength)));
    }
    elseif ($trimElement === 'word') {
      $teaser = substr($text, 0, strpos($text, ' ', $trimLength));
      $expandText = substr($text, strlen(substr($text, 0, strpos($text, ' ', $trimLength))));
    }
    else {
      $sentenceEnds = array('.', '?', '!');
      $newTrimLength = strlen($text);
      foreach ($sentenceEnds as $sentenceEnd) {
        $position = strpos($text, $sentenceEnd, $trimLength);
        if ($position > $trimLength && $position < $newTrimLength) {
          $newTrimLength = $position + 1;
        }
      }

      $teaser = substr($text, 0, $newTrimLength);
      $expandText = substr($text, strlen(substr($text, 0, $newTrimLength)));
    }

    $text = array(
      'Text' => $text,
      'Teaser' => $teaser,
      'ExpandText' => $expandText,
    );

    return $text;
  }

}
