(function ($, Drupal, window) {

  Drupal.behaviors.expandFormatter = {
    attach: function (context, settings) {
      var expandFormatters = $('.expand-formatter', context);

      expandFormatters.each(function(){
        var teaser = $('span.teaser', this);
        var expandTrigger = $('a.expandtrigger', this);
        var expandText = $('span.expandtext', this);
        var collapseTrigger = $('a.collapsetrigger', this);

        expandTrigger.click(function() {
          if(expandTrigger.hasClass('hidden') == false) {
            expandText.removeClass('hidden');
            expandTrigger.addClass('hidden');
            collapseTrigger.removeClass('hidden');
          }
        });

        collapseTrigger.click(function() {
          if(collapseTrigger.hasClass('hidden') == false) {
            expandText.addClass('hidden');
            expandTrigger.removeClass('hidden');
            collapseTrigger.addClass('hidden');
          }
        });

      });
    }
  }

})(jQuery, Drupal, window);
